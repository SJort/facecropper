import os
import shutil

import face_recognition
from PIL import Image

source_folder = "input/"
result_folder = "input_cropped/"
scanned_folder = "input_scanned/"


def face_scanner(filename):
    path = source_folder + filename
    image_ndarray = face_recognition.load_image_file(path)
    face_locations = face_recognition.face_locations(img=image_ndarray, number_of_times_to_upsample=1)

    print(f"{len(face_locations)} faces in {path}: {face_locations}")
    original_image = Image.open(path)
    count = 0
    for face_location in face_locations:
        left = face_location[3]
        top = face_location[0]
        right = face_location[1]
        bottom = face_location[2]
        cropped_image = original_image.crop((left, top, right, bottom))
        save_path = result_folder + filename
        cropped_image.save(save_path)
        count += 1
        print(f"Saved face {count} from {path}")

    original_image = None  # clear variable out of memory, otherwise cant move
    shutil.move(path, scanned_folder + filename)


filenames = []
for filename in os.listdir(source_folder):
    if not filename.endswith(".jpg") and not filename.endswith(".pgn"):
        continue
    filenames.append(filename)

print(f"{len(filenames)} images found")
for filename in filenames:
    print(f"Iterating {filename}")
    face_scanner(filename)

print(f"Iterated all image files in {source_folder}!")
