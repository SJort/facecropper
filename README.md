# FaceCropper
A quickly made Python script that crops faces from images in a folder and saves them.

## Windows installation
Install cmake
* Download https://visualstudio.microsoft.com/visual-cpp-build-tools/
* Install 'Desktop development with C++'

Install face_recognition
* Clone: https://github.com/davisking/dlib
* Follow in README: 'Compiling dlib Python API'
* pip install setuptools

# ImageBorderer
A quickly made Python script which puts images around a base image as border. The base image is overwritten. Expects square images, for example those as a result of the above script.
