"""
Around an image, creates a border of other images
"""
import os
from PIL import Image
from datetime import datetime
import random

in_image = "C:/jort/projects/cinthia/background.jpg"
in_folder = "C:/jort/projects/cinthia/input_cropped"
image_size = (200, 200)
border_amount = 4

timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
out_image = f"{timestamp}_bordered_image.jpg"


def printt(*argss, **kwargs):
    timestamp = datetime.now().strftime('%Y%m%d_%H%M%S')
    to_print = " ".join(map(str, argss))
    to_print = f"{timestamp}: {to_print}"
    print(to_print, **kwargs)


def load_images(folder_path):
    images = []
    for filename in os.listdir(folder_path):
        if filename.endswith(".jpg") or filename.endswith(".png"):
            img_path = os.path.join(folder_path, filename)
            img = Image.open(img_path).resize(image_size)
            images.append(img)
    printt(f"Loaded {len(images)} images from {folder_path}")
    return images


def place_image(result_image, image, x, y):
    print(f"Placing image at {x}, {y}")
    result_image.paste(image, (x, y))


printt("Started")
images = load_images(in_folder)
random.shuffle(images)

# images = images[:-25]
# images = images[:int(len(images) * 0.4)]
# images = list(range(1, 50))

background = Image.open(in_image)
result_image = background.copy()
bg_width, bg_height = background.size
img_width, img_height = image_size
horizontal_blocks = bg_width // img_width
vertical_blocks = bg_height // img_height
# horizontal_blocks = 5
# vertical_blocks = 5
printt(f"Using base image of {bg_width}x{bg_height} to fit {horizontal_blocks}x{vertical_blocks} images")

start_x = 0
start_y = 0
end_x = horizontal_blocks
end_y = vertical_blocks
lapse_count = 0
cursor_x = 0
cursor_y = 0
amount_of_places_images = 0

direction = "right"
for image in images:
    place_image(result_image, image, cursor_x * img_width, cursor_y * img_height)
    amount_of_places_images += 1
    if direction == "right":
        cursor_x += 1
        if cursor_x >= end_x - 1:
            direction = "down"
    elif direction == "down":
        cursor_y += 1
        if cursor_y >= end_y - 1:
            direction = "left"
    elif direction == "left":
        cursor_x -= 1
        if cursor_x <= start_x:
            direction = "up"
    elif direction == "up":
        cursor_y -= 1
        if cursor_y <= start_y - 1:
            direction = "right"
            printt(f"LAPSE")
            start_x += 1
            start_y += 1
            end_x -= 1
            end_y -= 1
            cursor_x = start_x
            cursor_y = start_y
            lapse_count += 1
            if lapse_count >= border_amount:
                printt(f"Desired amount of borders reached")
                break
    printt(f"Cursor at {cursor_x}.{cursor_y} going {direction}")

result_image.save(out_image)
printt(f"Placed {amount_of_places_images} images")
printt(f"Done, result saved as {out_image}")
